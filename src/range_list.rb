# Task: Implement a class named 'RangeList'
# A pair of integers define a range, for example: [1, 5). This range includes inte gers: 1, 2, 3, and 4.
# A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)
# NOTE: Feel free to add any extra member variables/functions you like.
require_relative 'my_range'

class RangeList
  attr_accessor :list

  def empty?
    list.empty?
  end

  def initialize
    self.list = []
  end

  def add(range)
    _add_r = MyRange.new(range)
    return list.push(_add_r) if empty?

    _temp_queue = []
    _cur_r = nil
    while list.length > 0
      _cur_r = list.shift
      if _add_r.end < _cur_r.begin
        _temp_queue.push(_add_r)
        _add_r = _cur_r
      elsif _add_r.begin > _cur_r.end
        _temp_queue.push(_cur_r)
      else
        # the two range are crossed, so we can simply use the min and max.
        minmax_range = [_add_r.begin, _cur_r.begin, _add_r.end, _cur_r.end].minmax
        _add_r = MyRange.new(minmax_range)
      end
    end
    self.list = _temp_queue
    list.push(_add_r) unless _add_r.nil?
  end

  def remove(range)
    return if empty?
    return if range[0] == range[1]

    _remove_r = MyRange.new(range)
    _temp_queue = []
    _cur_r = nil
    # iterate the list and remove the intersection between _remove_r and _cur_r
    while list.length > 0
      _cur_r = list.shift
      next _temp_queue.push(_cur_r) if _remove_r.end < _cur_r.begin || _remove_r.begin > _cur_r.end

      if _remove_r.begin <= _cur_r.begin && _remove_r.end <= _cur_r.end
        _temp_queue.push(MyRange.new([_remove_r.end, _cur_r.end]))
      elsif _remove_r.begin >= _cur_r.begin && _remove_r.end >= _cur_r.end
        _temp_queue.push(MyRange.new([_cur_r.begin, _remove_r.begin]))
      elsif _remove_r.begin <= _cur_r.begin && _remove_r.end >= _cur_r.end
        # cover, just do nothing
      else
        _temp_queue.push(MyRange.new([_cur_r.begin, _remove_r.begin]))
        _temp_queue.push(MyRange.new([_remove_r.end, _cur_r.end]))
      end
    end
    self.list = _temp_queue
  end

  def print_str
    list.map(&:to_s).join(' ')
  end

  def print
    puts print_str
  end
end
