require_relative '..\src\range_list'
# require 'byebug'

def test_case(rl, expect)
  rl.print
  puts rl.print_str == expect
end

def test_exception_case(_expect)
  yield
rescue StandardError => e
  puts "expect: #{_expect}"
  puts e.message == _expect
end

rl = RangeList.new

puts 'test add'
rl.add([1, 5])
test_case(rl, '[1, 5)')

rl.add([10, 20])
test_case(rl, '[1, 5) [10, 20)')

rl.add([20, 20])
test_case(rl, '[1, 5) [10, 20)')

rl.add([20, 21])
test_case(rl, '[1, 5) [10, 21)')

rl.add([2, 4])
test_case(rl, '[1, 5) [10, 21)')

rl.add([3, 8])
test_case(rl, '[1, 8) [10, 21)')

puts 'test remove'

rl.remove([10, 10])
test_case(rl, '[1, 8) [10, 21)')

rl.remove([10, 11])
test_case(rl, '[1, 8) [11, 21)')

rl.remove([15, 17])
test_case(rl, '[1, 8) [11, 15) [17, 21)')

rl.remove([3, 19])
test_case(rl, '[1, 3) [19, 21)')

puts 'additional tests'

rl_2 = RangeList.new
# have no range, directly remove.
rl_2.remove([3, 19])
test_case(rl_2, '')

rl_2.add([0, 0])
test_case(rl_2, '[0, 0)')

rl_2.remove([0, 0])
test_case(rl_2, '[0, 0)')

rl_2.add([-10, 1])
test_case(rl_2, '[-10, 1)')

rl_2.remove([-110, 190])
test_case(rl_2, '')

test_exception_case('uncaught throw "error: param range should be an array and includes two integers. such as [1, 5]"') do
  rl_2.add(['10', {}])
end

test_exception_case('uncaught throw "error: param range should be an array and includes two integers. such as [1, 5]"') do
  rl_2.add([10])
end

test_exception_case('uncaught throw "error: [10, -100] first number should not bigger than second."') do
  rl_2.add([10, -100])
end
