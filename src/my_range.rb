# extend Ruby core class Range
class MyRange < Range
  # range is an array, like [1, 5]
  def initialize(range)
    check_param(range)
    super(range[0], range[1], false)
  end

  def correct?(range)
    range.instance_of?(Array) && range[0].instance_of?(Integer) && range[1].instance_of?(Integer)
  end

  def check_param(range)
    throw 'error: param range should be an array and includes two integers. such as [1, 5]' unless correct?(range)
    throw "error: #{range.inspect} first number should not bigger than second." if range[0] > range[1]
  end

  def to_s
    "[#{self.begin}, #{self.end})"
  end
end
