# RangeList Test 

## Requirement

RangeList
In this part of the interview process, we'd like you to come up with an algorithm to
solve the problem as described below. The problem itself is quite simple to solve.

The solution needs to be in Ruby. What we are mainly looking for in this test (other
than that the solution should work) is, how well you actually write the code.

When you are done, please submit your code to gitlab.com as a public git repo and
email us the link.

We want to see how you write production-quality code in a team setting.
Specifically, we are looking for: simple, clean, readable and maintainable code, for

example:
Code organization and submission format.
Things like code organization, readability, documentation, testing and deliverability are most important here.

Your mastery of idiomatic Ruby programming.

We understand that you may not have much experience with Ruby. We encourage you to take some time to
research modern Ruby and best practices, and try your best to apply them when
writing your test solution.

## Solution

```sh
ruby test/range_list_test.rb
```
